describe('the map of consortium members', () => {

  var members = 66;
  // The number of additional markers because of members with multiple (2+) locations
  // September 2023: University of Potsdam, Leibniz University Hannover, Technical University of Cottbus (3), Free University of Berlin
  var multipleLocations = 5;
  var markers = members + multipleLocations;

  beforeEach(() => {
    cy.visit('index.html');
  });

  const checkFeatures = (features => {
    debugger;
    expect(features[0].geometry.type).to.equal('Marker');
    expect(features[0].geometry.coordinates.length).to.equal(2);
  });

  it('should have the map and all buttons', () => {
    cy.get('#divMap').should('exist');
    cy.get('#resetMapButton').should('exist');
    cy.get('.leaflet-control-zoom-in').should('exist');
    cy.get('.leaflet-control-zoom-out').should('exist');
  });

  it('should list all members on the map', () => {
    cy.window().wait(3000).then(({ map }) => {
      var markerIDs = [];
      map.eachLayer(function (layer) {
        if (layer.hasOwnProperty('options') && layer.options.hasOwnProperty('id')) {
          markerIDs.push(layer.options.id);
        }
      });
      expect(markerIDs).to.have.length(markers);
    });
  });

  it('should open a popup that is not empty when each marker is clicked', () => {
    // The markers can't be clicked individually because some are hiding others, e.g. in Munich
    // Solution: Get the popup's content of each layer and check if it contains 'namePopup'

    cy.window().wait(3000).then(({ map }) => {
      var contentArray = [];
      map.eachLayer(function (layer) {
        if (layer.hasOwnProperty('options') && layer.options.hasOwnProperty('id')) {
          var content = layer['_popup']['_content'];
          expect(content).to.contain('namePopup');
          contentArray.push(content);
        }
      });
      expect(contentArray).to.have.length(markers);
    });

  });

  it('should open the correct popup when AWI marker is clicked', () => {
    cy.window().wait(3000).then(({ map }) => {
      map.eachLayer(function (layer) {
        if (layer.hasOwnProperty('options') && layer.options.hasOwnProperty('id') && layer['_latlng']['lat'] == 53.532722 && layer['_latlng']['lng'] == 8.579556) {
          cy.get(layer['_icon']).click({force: true});
          cy.get('.namePopup').should('contain', 'Alfred Wegener Institute');
        }
      });
    });

  });

});
